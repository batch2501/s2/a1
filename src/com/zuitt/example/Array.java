package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // [SECTION] Java Collection
        // are single unit of objects
        // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops.

    public static void main(String [] args){
        // [SECTION] Array
            // In Java, arrays are containers of values of the same type given a predefined amount a=of values.
            // Java arrays are more rigid. Once the size and data type are defined, they can no longer be changed.
        // Array declaration
            // dataType[] identifier = new dataType[numOfElements];
                // "[]" indicates that the data type should be able to hold multiple values.
                // "new" keyword is for the non-primitive data types to tell Java to create the said variable.
                // the values of the array is initialized either with a 0 or null.
        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
        // intArray[5] = 100; // out of bounds error

        // this will return the memory address of the array
        //System.out.println(intArray);

        // To print the intArray, we need to import the Array class and use the ".toString()" method to convert the array into string when we print it into the terminal
        System.out.println(Arrays.toString(intArray));

        // Declaration with initialization
            // Syntax:
            // dataType[] identifier = {elementA, elementB, elementC, ...};
                // the compiler automatically specifies the size by counting the number of elements in the array
        String[] names = {"John","James","Joe"};
        // names[3] = "Joey";
        System.out.println(Arrays.toString(names));

        // Sample Java Array methods:
        // Sort:
        Arrays.sort(intArray);
        System.out.println("Order of items after the sort method" + Arrays.toString(intArray));

        // Multidimensional arrays
            // A two-dimensional array can be described by two lengths nested within each other, like a matrix.

        String[][] classroom = new String[3][3];

        // First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        // Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";

        // Third Row
        classroom[2][0] = "Mickey Mouse";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        // we use the deepToString() method in accessing values for multidimensional arrays.
        System.out.println(Arrays.deepToString(classroom));

        // in java, the size of the array cannot be modified. if there is a need to add or remove elements, new arrays must be created.

        //[SECTION] ArrayLists
            // are resizable arrays, wherein elements can be added or removed whenever it is needed.

            // Syntax:
                // ArrayList<T> identifier = new ArrayList<T>();
                // "<T>" is used to specify that the list can only have one type of object in a collection
                // ArrayList cannot hold primitive data types, "Java wrapper class" provides a way to use these types of data as object.
                    // in short, object version of primitive data types with method

        //Declaring an ArrayList
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        //Declaring an ArrayList then initializing
        //ArrayList<String> students = new ArrayList<String>();

        //Declaring an ArrayList with values
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane","Mike"));

        // Add elements
        //Syntax: arrayListName.add(element);
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        // Access an element/s
        //arrayListName.get(index);
        System.out.println(students.get(0));
        // System.out.println(students.get(5));  //error: out of bounds

        //Add an element to a specific index.
        // arrayListName.add(index, element);
        students.add(0,"Joey");
        System.out.println(students);

        //Updating an element
        // arrayListName.set(index, element);
        students.set(0,"George");
        System.out.println(students);

        // Removing a specific element
        //arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        // Removing all elements
        students.clear();
        System.out.println(students);

        // Getting the arrayList size
        System.out.println((students.size()));

        // [SECTION] Hashmaps
            // most objects in Java are defined and are instantiations of classes that contain a set of properties and methods.
            // Syntax:
                // Hashmap<dataTypeField, dataTypeValue>

        //Declaring Hashmaps
        //HashMap<String, String> jobPosition = new HashMap<String, String>();

        //Declaring Hashmap with initialization
        HashMap<String, String> jobPosition = new HashMap<String, String>(){
            {
                put("Teacher","John");
                put("Artists","Jane");
            }
        };
        System.out.println(jobPosition);

        // Add elements
        // Hashmaps.put(<fieldName>,<value>);
        jobPosition.put("Student","Brandon");
        jobPosition.put("Dreamer","Alice");
        System.out.println(jobPosition);

        // Accessing an element -use the field name
        System.out.println(jobPosition.get("Student"));
        // System.out.println(jobPosition.get("student"));
        // System.out.println(jobPosition.get("Admin"));

        //Updating the value
        // hashmapName.replace("fieldNameToChange","newValue");
        jobPosition.replace("Student","Brandon Smith");
        System.out.println(jobPosition);

        // Removing an element
        // hashmapName.remove(key);
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        // Remove all elements
        // hashmapName.clear();
        jobPosition.clear();
        System.out.println(jobPosition);

    }
}
