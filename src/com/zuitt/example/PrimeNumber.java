package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args){
        int[] primeNumbers = {2,3,5,7,11};
        Scanner input = new Scanner(System.in);
        System.out.println("Enter an index to display the prime number");
        int index = input.nextInt();

        switch(index){
            case 0:
                System.out.println("The element at index " + index + " is " + primeNumbers[index]);
                break;
            case 1:
                System.out.println("The element at index " + index + " is " + primeNumbers[index]);
                break;
            case 2:
                System.out.println("The element at index " + index + " is " + primeNumbers[index]);
                break;
            case 3:
                System.out.println("The element at index " + index + " is " + primeNumbers[index]);
                break;
            case 4:
                System.out.println("The element at index " + index + " is " + primeNumbers[index]);
                break;
            default:
                System.out.println("Invalid index. Please enter an index between 0 to 4");
                break;

        }

        // Array List
        List<String> friends = new ArrayList<>(List.of("John","Jane","Chloe","Zoey"));
        System.out.println("My friends are " + friends);

        //hashmap
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Toothpaste", 15);
        map.put("Soap", 25);
        map.put("Shampoo", 13);
        System.out.println("Contents of the HashMap: " + map);

    }
}
