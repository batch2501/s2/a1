package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String [] args){
        Scanner leapYear = new Scanner(System.in);
        System.out.println("enter a year:");
        int year = leapYear.nextInt();

        Boolean isLeapYear = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;

        System.out.println(year + (isLeapYear ? " is " : " is not ") + "a leap year");
    }
}
